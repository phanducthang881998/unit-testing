import java.util.ArrayList;
import java.util.List;

public class Stack<T> implements StackInterface<T>{
	private List<T> mList = new ArrayList<T>();

    @Override
    public boolean isEmpty() {
        return mList.isEmpty();
    }

    @Override
    public void makeEmpty() {
    	mList = new ArrayList<>();
    }

    @Override
    public void pop() {
        try {
            if (mList.size() <= 0) {
                throw new Exception();
            }
            mList.remove(mList.size() - 1);
        } catch (Exception e) {
            System.out.println("Stack is Empty");
        }
    }

    @Override
    public void push(T value) {
        try {
            if(mList.size() >= Integer.MAX_VALUE){
                throw new Exception();
            }
            mList.add(value);
        }catch (Exception e){
            System.out.println("Stack is max size");
        }

    }

    @Override
    public T top() {
        try{
            if(mList.size() <=0){
                throw new Exception();
            }
        return mList.get(mList.size() - 1);
        }catch (Exception e) {
            System.out.println("Stack is Empty!!!!");
            return null;
        }
    }

    @Override
    public int length() {
        return mList.size();
    }
}
